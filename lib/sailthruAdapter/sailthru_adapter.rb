require "sailthruAdapter/version"
require "sailthruAdapter/sqs_recvr"

module SailthruAdapter
  class Adapter

    attr_reader :running, :finished

    def initialize(sailthruClient, sqsConfig)
      @sqsRecvr = SqsRecvr.new(sqsConfig.queue_url, sailthruClient)
      @running = false
      @finished = false
    end

    def startAndWait
      start

      @t1.join unless @t1.nil?
      puts "exiting startAndWait"
    end

    def start
      fail "Already Finished" if @finished
      fail "Already Running" if @running
      stop_polling = -> { @finished }
      @t1 = @sqsRecvr.recv(stop_polling)
      puts "T1 is #{@t1}"
      @running=true
      @t1
    end

    def stop
      fail "Already Finished" if @finished
      fail "Not Running" if !@running
      @finished = true
      @t1.join()
    end
  end
end
