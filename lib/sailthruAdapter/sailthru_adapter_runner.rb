require 'sailthruAdapter/sailthru_adapter'
require 'sailthruAdapter/config_sqs'
require 'sailthruAdapter/config_sailthru'
module SailthruAdapter
  class AdapterRunner

      def initialize()
        sqsConfig = AwsSqsConfig.new
        sailthruClient = EnvSailthruConfig.new

        adapter = Adapter.new(sqsConfig, sailthruClient)
        adapter.start()
      end
  end
end
