module SailthruAdapter
  module EnvSupport
    def getEnv(name)
      value = ENV[name]
      fail "Cannot read value for environmental variable [#{name}]" if value.nil? || value.strip.length==0
      value
    end
  end
end
