class MyRecvr
  def self.recv (queueURL, on_message, stop_polling)
    poller = Aws::SQS::QueuePoller.new(queueURL, {})
    poller.before_request do |stats|
      throw :stop_polling if stop_polling.call
    end
    Thread.new {
      poller.poll(wait_time_seconds:5) do |msg|
        on_message.call(msg)
      end
    }
  end
end

