# coding: utf-8
lib = File.expand_path('../lib', __FILE__)
$LOAD_PATH.unshift(lib) unless $LOAD_PATH.include?(lib)
require 'sailthruAdapter/version'

Gem::Specification.new do |spec|
  spec.name          = "sailthruAdapter"
  spec.version       = SailthruAdapter::VERSION
  spec.licenses      = ['Nonstandard']
  spec.authors       = ["David Hay"]
  spec.email         = ["david.j.hay@gmail.com"]

  spec.summary       = %q{Sailthru Adapter}
  spec.description   = %q{Loops, reads messages from SQS queue, for each messgage calls sailthruApi with appropriate parameters.}
  spec.homepage      = "http://homepage"

  # Prevent pushing this gem to RubyGems.org by setting 'allowed_push_host', or
  # delete this section to allow pushing this gem to any host.
  if spec.respond_to?(:metadata)
    #spec.metadata['allowed_push_host'] = "TODO: Set to 'http://mygemserver.com'"
  else
    raise "RubyGems 2.0 or newer is required to protect against public gem pushes."
  end

  spec.files         = `git ls-files -z`.split("\x0").reject { |f| f.match(%r{^(test|spec|features)/}) }
  spec.bindir        = "exe"
  spec.executables   = spec.files.grep(%r{^exe/}) { |f| File.basename(f) }
  spec.require_paths = ["lib"]

  spec.add_development_dependency 'bundler', '~> 1.11', '>= 1.11.2'
  spec.add_development_dependency 'rake', '~> 10.4', '>= 10.4.2'
  spec.add_development_dependency "aws-sdk", "~> 2.2", ">= 2.2.31"
  spec.add_development_dependency "fake_sqs", "~> 0.3.1"
  spec.add_development_dependency 'test-unit', '~> 3.0', '>= 3.0.8'
  spec.add_development_dependency 'rspec', '~> 3.4', '>= 3.4.0'
  spec.add_development_dependency 'json-schema', '~> 2.6', '>= 2.6.1'
  spec.add_development_dependency 'sailthru-client', '~> 4.0', '>= 4.0.7'
  spec.add_development_dependency 'logging', '~> 2.1', '>= 2.1.0'

end

