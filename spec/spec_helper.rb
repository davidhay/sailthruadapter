require 'ostruct'
require 'aws-sdk'
require 'test/unit'
require 'fake_sqs/test_integration'

#Test::Unit::AutoRunner.need_auto_run = false if defined?(Test::Unit::AutoRunner)
Test::Unit::AutoRunner.need_auto_run = false

FAKE = OpenStruct.new
FAKE.access_key_id='Fake'
FAKE.secret_access_key='Fake'
FAKE.region='Fake'
FAKE.host="localhost"
FAKE.port = 1234

Aws.config.update(
    access_key_id: FAKE.access_key_id,
    secret_access_key: FAKE.secret_access_key,
    region: FAKE.region
)

RSpec.configure do |config|
  config.before(:suite) {
    $fake_sqs = FakeSQS::TestIntegration.new(
        database: ":memory:",
        sqs_endpoint: FAKE.host,
        sqs_port: FAKE.port
    )
    $client = Aws::SQS::Client.new(region: FAKE.region)
    $client.config.endpoint = $fake_sqs.uri
  }
  config.before(:each, :sqs) { $fake_sqs.start }
  config.after(:suite) { $fake_sqs.stop }
end
