require "sailthruAdapter/version"
require 'sailthruAdapter/config_sqs'
require 'sailthruAdapter/config_sailthru'
require 'sailthruAdapter/sailthru_client_factory'
require "sailthruAdapter/sailthru_adapter"

module SailthruAdapter

  class Runner
    def run
      sqsConfig = AwsSqsConfig.new
      sailthruConfig = ConfigSailthruEnv.new
      sailthruClient = SailthruClientFactory.new(sailthruConfig).createSailthruClient

      adapter = Adapter.new(sailthruClient, sqsConfig)
      adapter.startAndWait
      puts "Runner exiting"
    end
  end

end
